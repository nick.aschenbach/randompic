# Description

A small spike to create random images in both png and jpg format in different qualities (for the latter). The goal was to generate data about at how the file sizes change. My thinking is that random pictures was a worst case scenario because there may be more information than in a typical profile image.

