const Jimp = require("jimp");
const { execSync } = require("child_process");
const uuid = require("uuid").v4;
var fs = require("fs");

const width = 200;
const height = 200;

const createImage = async () => {
  return new Promise((resolve, reject) => {
    new Jimp(width, height, (err, image) => {
      if (err) reject(err);

      resolve(image);
    });
  });
};

const scan = async (image) => {
  return new Promise((resolve) => {
    image.scan(0, 0, image.bitmap.width, image.bitmap.height, function (
      x,
      y,
      idx
    ) {
      this.bitmap.data[idx + 0] = Number.parseInt(Math.random() * 255);
      this.bitmap.data[idx + 1] = Number.parseInt(Math.random() * 255);
      this.bitmap.data[idx + 2] = Number.parseInt(Math.random() * 255);
      this.bitmap.data[idx + 3] = 255;

      resolve(image);
    });
  });
};

const save = ({ image, filename, quality }) => {
  return new Promise((resolve, reject) => {
    image.quality(quality);
    image.write(`${filename}`, (err, data) => {
      if (err) reject(err);

      resolve();
    });
  });
};

const createRandomImage = async ({ format, quality }) => {
  let image = await createImage();
  image = await scan(image);
  const filename = `${uuid()}.${format}`;
  await save({ image, filename, quality });
  const stats = fs.statSync(filename);
  const fileSize = stats.size;

  const base64 = execSync(`base64 ${filename} | wc`);
  const parts = base64.toString().split(" ");
  const encodedSize = parts[parts.length - 1].trim();

  console.log(`${format},${quality},${fileSize},${encodedSize},${filename}`);
};

const createRandomImageSets = async () => {
  execSync("rm *.png *.jpg");

  console.log("format,quality,filesize,base64size,filename");
  const formats = ["png", "jpg"];
  const qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

  formats.map(async (format) => {
    qualities.map(async (quality) => {
      for (let i = 0; i < 1; i++) await createRandomImage({ format, quality });
    });
  });
};

createRandomImageSets();
